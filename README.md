This is an implementation of an interpreter(evaluator) for a simple untyped lambda calculus (the computational model underlying functional programming languages such as Haskell). 

Abstract Syntax
================
Specifically, an endofunctor has been defined that includes the variants for arithmetic expressions along with the following functional language constructs:

* **lambda x . e:** anonymous function, that is, a designated formal argument x along with a function body in which x occurs zero or more times
* **x:** variable (presumably occurring in an expression with ancestor that is a lambda expression introducing x as a formal argument)
* **e1 e2:** function application, that is, an expression (presumably representing a function) followed by another expression (presumably representing an actual argument)
* **if c then e1 else e2:** conditional.



* **Cell (::):** this constructor takes two arguments, both of which are arbitrary expressions. Thus, Cell allows you to build not only lists but, by using another Cell value as the first argument, arbitrarily nested tree structures (see also http://en.wikipedia.org/wiki/Cons). Note that we will not add a separate Nil value because we can already express Nil using existing constructs. (In LISP, these cells are known as "cons" cells, but we call this construct Cell for easier distinction from Const.)
* **Hd and Tl:** each of these destructors (accessors) takes one argument, which is an arbitrary expression.
* **IsCell:** this accessor takes one argument, which is an arbitrary expression. 


Semantics
==========
The semantic domain (values) is a subdomain of the abstract syntactic domain consisting only of

- integer constants
- function closures

Next, a function has been defined for evaluating expressions represented using the abstract syntax defined above using normal-order evaluation (a non-strict evaluation strategy). This function takes an expression that follows the abstract syntax defined above and reduces it to a value from the semantic domain.

- An arithmetic expression involving non-arithmetic values is an error.
- An anonymous function definition evaluates to itself.
- An (unbound) variable by itself is an error.
- A function application is evaluated as follows (this is where the evaluation order is decided):
	- The first expression is evaluated. (If it the result is not a function closure, **lambda x . b**, then there is an error.)
	- Beta substitution is performed to substitute the second expression (without evaluating it) for every free occurrence of x in b.
	- The resulting function body (after the substitution) is evaluated.


- A conditional is evaluated as follows. (Note that it is non-strict in e1 and e2, otherwise it would defeat the purpose.)
	- The condition is evaluated.
	- If the result the integer constant 0, then the result is the result of evaluating e2.
	- Otherwise, if the condition is non-0, the result is the result of evaluating e1.



- A *Cell* expression evaluates to itself. That is, the children are evaluated on demand following normal-order evaluation.
- *Hd* evaluates its argument, expecting a Cell, and then evaluates the first child of the Cell.
- *Tl* evaluates its argument, expecting a Cell, and then evaluates the second child of the Cell.
- *IsCell* evaluates its argument and indicates whether or not the result is a Cell.
	IsCell(Cell(...)) -> Const(1)
	IsCell(Const(...)) -> Const(0)
	IsCell(Fun(...)) -> Const(0)


Substitution Examples
=====================
	(λy.y + ((λx.x) 3) + x)[5/x]   // branch 3: y not free in a = 5
	-> λy.y[5/x] + ((λx.x) 3)[5/x] + x[5/x]
	-> λy.y + ((λx.x)[5/x] 3[5/x]) + 5
	-> λy.y + ((λx.x) 3) + 5  // branch 1: y = x


	(λy.y + ((λx.x) 3) + x)[y/x]   // branch 2: y free in a = y
	-> (λy'.y' + ((λx.x) 3) + x)[y/x]
	-> λy'.y' + ((λx.x) 3) + y



Concrete Examples
==================
	eval(Const(3)) -> Const(3)
	eval(Var("x")) -> error
	eval(Fun("x", Plus(Const(7), Var("x")))) 
  		-> Fun("x", Plus(Const(7), Var("x")))
	eval(App(Fun("x", Plus(Const(7), Var("x"))), Const(3)))
  		-> Const(10)

	eval(App(Var("x"), Const(3)) -> error
	eval(If(Const(7), Const(3), Const(4))) -> Const(3)
	eval(If(Const(0), Const(3), Const(4))) -> Const(4)
	eval(If(Fun("x", Var("x")), Const(3), Const(4))) -> Const(3)
	eval(If(Fun("x", Var("y")), Const(3), Const(4))) -> Const(3)
	
	val Y = // you'll define it yourself as in the handout!
	eval(App(App(Y, Fun("f", Fun("n", If(Var("n"),
  	Times(Var("n"), App(Var("f"), Minus(Var("n"), Const(1)))), Const(1))))), Const(5)))
  		-> Const(120)


	eval(Cell(Plus(Const 3, Const 7), Minus(Const 5, Const 2))) -> Cell(Plus(Const 3, Const 7), Minus(Const 5, Const 2))
	eval(Hd(Cell(Plus(Const 3, Const 7), Minus(Const 5, Const 2)))) -> Const 10
	eval(Tl(Cell(Plus(Const 3, Const 7), Minus(Const 5, Const 2)))) -> Const 3
	eval(If(Cell(...), Const 3, Const 4)) -> Const 3
	eval(Hd(Const 0)) -> error
	eval(Tl(Const 0)) -> error
	eval(Tl(Fun("x", Var("x")))) -> error
	eval(Tl(Cell(Const 10, Fun("x", Var("x"))))) -> Fun("x", Var("x"))
