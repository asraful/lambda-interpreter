package project3b

import scalamu.In
import scala.util.Random


object behaviors {

  import structures._
  import structures.ExprFactory._


  def eval(expr: Expr): Either[String, Expr] = expr match {
    case In(Constant(c)) => Right(constant(c))
    case In(Var(x))      => Left("Error")
    case In(Fun(x, e))   => Right(fun(x, e))

    case In(App(e1, e2)) => eval(e1) match {
      case Right(In(Fun(x, e))) => eval(substitute(e, x, e2))
      case Right(In(_))  => Left("Error")
      case Left(text)    => Left(text)
    }

    case In(If(cond, e1, e2)) => eval(cond) match {
      case Right(In(Constant(0))) => eval(e2)
      case Right(In(_)) => eval(e1)
      case Left(text) => Left(text)
    }

    case In(UMinus(r))   => calculate(uminus(r))
    case In(Plus(l, r))  => calculate(plus(l, r))
    case In(Minus(l, r)) => calculate(minus(l, r))
    case In(Times(l, r)) => calculate(times(l, r))
    case In(Div(l, r))   => calculate(div(l, r))
    case In(Mod(l, r))   => calculate(mod(l, r))


    case In(Cell(e1, e2)) =>  Right(cell(e1, e2))

    case In(Hd(e)) => eval(e) match {
      case Right(In(Cell(e1, e2))) => eval(e1)
      case Right(In(_)) => Left("Error")
      case Left(text)   => Left(text)
    }

    case In(Tl(e)) => eval(e) match {
      case Right(In(Cell(e1, e2))) => eval(e2)
      case Right(In(_)) => Left("Error")
      case Left(text)   => Left(text)
    }

    case In(IsCell(e)) =>  eval(e) match {
      case Right(In(Cell(e1, e2))) => Right(constant(1))
      case _ =>  Right(constant(0))
    }

  }


  def substitute(e1: Expr, arg: String, e2: Expr): Expr = e1 match {
    case In(Constant(c))     =>  constant(c)

    case In(Var(x))     if(x == arg)      =>  e2
    case In(Var(x))                       =>  variable(x)

    case In(Fun(x, e))  if(x == arg)      =>  fun(x, e)
    case In(Fun(x, e))  if(x != arg && isFree(e, arg) && isFree(e2, x))  =>
        val xprime = newVar(x)
        fun(xprime, substitute((substitute(e, x, variable(xprime))), arg, e2))
    case In(Fun(x, e))                    =>  fun(x, substitute(e, arg, e2))

    case In(App(e11, e22))        =>  app(substitute(e11, arg, e2), substitute(e22, arg, e2))
    case In(If(cond, e11, e22))   =>  iff(substitute(cond, arg, e2), substitute(e11, arg, e2), substitute(e22, arg, e2))

    case In(UMinus(r))      =>  uminus(substitute(r, arg, e2))
    case In(Plus(l, r))     =>  plus(substitute(l, arg, e2), substitute(r, arg, e2))
    case In(Minus(l, r))    =>  minus(substitute(l, arg, e2), substitute(r, arg, e2))
    case In(Times(l, r))    =>  times(substitute(l, arg, e2), substitute(r, arg, e2))
    case In(Div(l, r))      =>  div(substitute(l, arg, e2), substitute(r, arg, e2))
    case In(Mod(l, r))      =>  mod(substitute(l, arg, e2), substitute(r, arg, e2))
    case In(App(e11, e22))  =>  app(substitute(e11, arg, e2), substitute(e22, arg, e2))

    case In(Cell(e1, e2))    =>  cell(substitute(e1, arg, e2),substitute(e2, arg, e2))
    case In(Hd(e))          =>  hd(substitute(e, arg, e2))
    case In(Tl(e))          =>  tl(substitute(e, arg, e2))
    case In(IsCell(e))      =>  isCell(substitute(e, arg, e2))

  }


  def newVar(x: String): String = {
    return x + Random.nextInt()
  }


  def isFree(e: Expr, arg: String): Boolean = e match {
    case In(Constant(c))      =>  false
    case In(Var(x))           =>  if (x == arg) true  else  false
    case In(Fun(x, expr))     =>  if(isFree(expr, arg))  true  else  false
    case In(If(c, e1, e2))    =>  if(isFree(e1, arg) || isFree(e2, arg))  true  else  false
    case In(App(e1, e2))      =>  if(isFree(e1, arg) || isFree(e2, arg))  true  else  false
    case In(Minus(e1, e2))    =>  if(isFree(e1, arg) || isFree(e2, arg))  true  else  false
    case In(Times(e1, e2))    =>  if(isFree(e1, arg) || isFree(e2, arg))  true  else  false
    case In(Plus(e1, e2))     =>  if(isFree(e1, arg) || isFree(e2, arg))  true  else  false
    case In(Cell(e1, e2))     =>  if(isFree(e1, arg) || isFree(e2, arg))  true  else  false
    case In(Hd(e))            =>  if(isFree(e, arg)) true  else  false
    case In(Tl(e))            =>  if(isFree(e, arg)) true  else  false
    case In(IsCell(e))        =>  if(isFree(e, arg)) true  else  false

  }



  def calculate(expr: Expr): Either[String, Expr] = expr match {
    case In(Constant(c))    =>  Right(constant(c))

    case In(UMinus(r))      =>  eval(r) match {
      case Right(In(Constant(c)))  =>  Right(constant(-c))
      case _                =>  Left("Error")
    }

    case In(Plus(l, r))           =>
      eval(l) match {
        case Right(In(Constant(c1)))     =>
          eval(r) match {
            case Right(In(Constant(c2))) =>   Right(constant(c1 + c2))
            case _                =>  Left("Error")
          }
        case _                =>   Left("Error")
      }

    case In(Minus(l, r))          =>
      eval(l) match {
        case Right(In(Constant(c1)))     =>
          eval(r) match {
            case Right(In(Constant(c2))) =>  Right(constant(c1 - c2))
            case _                =>  Left("Error")
          }
        case _                =>  Left("Error")
      }

    case In(Times(l, r))          =>
      eval(l) match {
        case Right(In(Constant(c1)))     =>
          eval(r) match {
            case Right(In(Constant(c2))) =>  Right(constant(c1 * c2))
            case _                =>  Left("Error")
          }
        case _                =>  Left("Error")
      }

    case In(Div(l, r))            =>
      eval(l) match {
        case Right(In(Constant(c1)))     =>
          eval(r) match {
            case Right(In(Constant(c2))) =>  Right(constant(c1 / c2))
            case _                =>  Left("Error")
          }
        case _                =>  Left("Error")
      }

    case In(Mod(l, r))            =>
      eval(l) match {
        case Right(In(Constant(c1)))     =>
          eval(r) match {
            case Right(In(Constant(c2))) =>  Right(constant(c1 % c2))
            case _                =>  Left("Error")
          }
        case _                =>  Left("Error")
      }

  }

}