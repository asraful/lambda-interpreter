package project3b

import scalaz.{ Equal, Functor, Show }
import scalamu._ // algebra types and injected cata method

/*
 * In this example, we represent arithmetic expressions as trees
 * (initial algebra for the endofunctor defined next).
 */

object structures {

  /**
   * Endofunctor for (nongeneric) F-algebra in the category Scala types.
   * Note that `A` is not a generic item type of the resulting algebraic
   * data type. As can be seen below, once we form `Expr` as the least
   * fixpoint of `ExprF`, `A` will go away.
   *
   * @tparam A argument of the endofunctor
   */
  sealed trait ExprF[+A]

  case class Constant(value: Int) extends ExprF[Nothing]
  case class UMinus[A](expr: A) extends ExprF[A]
  case class Plus[A](left: A, right: A) extends ExprF[A]
  case class Minus[A](left: A, right: A) extends ExprF[A]
  case class Times[A](left: A, right: A) extends ExprF[A]
  case class Div[A](left: A, right: A) extends ExprF[A]
  case class Mod[A](left: A, right: A) extends ExprF[A]

  case class Var(name: String) extends ExprF[Nothing]

  case class Fun[A](arg: String, body: Expr) extends ExprF[Nothing]
  case class App[A](func: A, arg:A) extends ExprF[A]
  case class If[A](c: A, e1: A, e2: A) extends ExprF[A]

  case class Cell[A](e1: Expr, e2: Expr) extends ExprF[Nothing]
  case class Hd[A](e: A) extends ExprF[A]
  case class Tl[A](e: A) extends ExprF[A]
  case class IsCell[A](e: A)  extends ExprF[A]




  /**
   * Implicit value for declaring `ExprF` as an instance of
   * typeclass `Functor` in scalaz. This requires us to define
   * `map`.
   */
  implicit object ExprFFunctor extends Functor[ExprF] {
    def map[A, B](fa: ExprF[A])(f: A => B): ExprF[B] = fa match {

        case Constant(v) => Constant(v)
        case UMinus(r)   => UMinus(f(r))
        case Plus(l, r)  => Plus(f(l), f(r))
        case Minus(l, r) => Minus(f(l), f(r))
        case Times(l, r) => Times(f(l), f(r))
        case Div(l, r)   => Div (f(l), f(r))
        case Mod(l, r)   => Mod (f(l), f(r))

        case Var(x)         => Var(x)
        case Fun(x, e)      => Fun(x, e)
        case App(e1, e2)    => App(f(e1), f(e2))
        case If(c, e1, e2)  => If(f(c), f(e1), f(e2))

        case Cell(e1, e2)  => Cell(e1, e2)
        case Hd(e)         => Hd(f(e))
        case Tl(e)         => Tl(f(e))
        case IsCell(e)     => IsCell(f(e))

    }
  }

  /**
   * Implicit value for declaring `ExprF` as an instance of
   * typeclass `Equal` in scalaz using `Equal`'s structural equality.
   * This enables `===` and `assert_===` on `ExprF` instances.
   */
  implicit def ExprFEqual[A]: Equal[ExprF[A]] = Equal.equalA

  /**
   * Implicit value for declaring `ExprF` as an instance of
   * typeclass `Show` in scalaz using `Show`'s default method.
   * This is required for `===` and `assert_===` to work on `ExprF` instances.
   */
  implicit def ExprFShow[A]: Show[ExprF[A]] = Show.showFromToString

  /**
   * Least fixpoint of `ExprF` as carrier object for the initial algebra.
   */
  type Expr = µ[ExprF]

  /**
   * Factory for creating Expr instances.
   */
  object ExprFactory {

    def constant(c: Int): Expr        = In(Constant(c))
    def uminus(r: Expr): Expr         = In(UMinus(r))
    def plus(l: Expr, r: Expr): Expr  = In(Plus (l, r))
    def minus(l: Expr, r: Expr): Expr = In(Minus(l, r))
    def times(l: Expr, r: Expr): Expr = In(Times(l, r))
    def div(l: Expr, r: Expr): Expr   = In(Div (l, r))
    def mod(l: Expr, r: Expr): Expr   = In(Mod (l, r))

    def variable(x: String): Expr               = In(Var(x))
    def fun(x: String, e: Expr): Expr           = In(Fun(x, e))
    def app(e1: Expr, e2: Expr): Expr           = In(App(e1, e2))
    def iff(c: Expr, e1: Expr, e2: Expr): Expr  = In(If(c, e1, e2))

    def cell(e1: Expr, e2: Expr): Expr = In(Cell(e1,e2))
    def hd(e: Expr): Expr              = In(Hd(e))
    def tl(e: Expr): Expr              = In(Tl(e))
    def isCell(e: Expr): Expr          = In(IsCell(e))

  }
}