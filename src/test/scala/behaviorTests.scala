/**
 * Created by Asraful on 3/27/2014.
 */

package project3b

import org.scalatest.FunSuite
import project3b.structures._

class behaviorTests extends FunSuite {

  import behaviors._
  import structures.ExprFactory._
  import fixtures._;


  test("eval works") {

    assert(eval(expr3a1)  ==  Right(constant(3)))
    assert(eval(expr3a2)  ==  Left("Error"))
    assert(eval(expr3a3)  ==  Right(fun("x", plus(constant(3), variable("x")))))
    assert(eval(expr3a4)  ==  Right(constant(10)))
    assert(eval(expr3a5)  ==  Left("Error"))
    assert(eval(expr3a6)  ==  Right(constant(3)))
    assert(eval(expr3a7)  ==  Right(constant(4)))
    assert(eval(expr3a8)  ==  Right(constant(3)))
    assert(eval(expr3a9)  ==  Right(constant(3)))
    assert(eval(expr3a10) ==  Right(constant(120)))
    assert(eval(expr3a11) ==  Right(constant(20)))
    assert(eval(expr3a12) ==  Left("Error"))



    /**
    New project3b tests.
     */
    assert(eval(expr3b1)  ==  Right(cell(plus(constant(3),constant(7)),minus(constant(5),constant(2)))))
    assert(eval(expr3b2)  ==  Right(constant(10)))
    assert(eval(expr3b3)  ==  Right(constant(3)))
    assert(eval(expr3b4)  ==  Right(constant(3)))
    assert(eval(expr3b5)  ==  Left("Error"))
    assert(eval(expr3b6)  ==  Left("Error"))
    assert(eval(expr3b7)  ==  Left("Error"))
    assert(eval(expr3b8)  ==  Right(fun("x",variable("x"))))


    assert(eval(app(app(Y, preLength), constant(0)))  ==  Right(constant(0)))
    assert(eval(app(app(Y, preLength), expr3b9))      ==  Right(constant(1)))
    assert(eval(app(app(Y, preLength), expr3b10))     ==  Right(constant(2)))
    assert(eval(app(app(Y, preLength), expr3b11))     ==  Right(constant(3)))
    assert(eval(app(app(Y, preLength), expr3b12))     ==  Right(constant(4)))
    assert(eval(app(app(Y, preLength), expr3b13))     ==  Right(constant(1)))
    assert(eval(app(app(Y, preLength), expr3b14))     ==  Right(constant(3)))
    assert(eval(app(app(Y, preLength), expr3b15))     ==  Right(constant(5)))


    assert(eval(app(app(Y, preSize), constant(0)))  ==  Right(constant(0)))
    assert(eval(app(app(Y, preSize), expr3b9))      ==  Right(constant(1)))
    assert(eval(app(app(Y, preSize), expr3b10))     ==  Right(constant(2)))
    assert(eval(app(app(Y, preSize), expr3b11))     ==  Right(constant(3)))
    assert(eval(app(app(Y, preSize), expr3b12))     ==  Right(constant(4)))
    assert(eval(app(app(Y, preSize), expr3b13))     ==  Right(constant(1)))
    assert(eval(app(app(Y, preSize), expr3b14))     ==  Right(constant(6)))
    assert(eval(app(app(Y, preSize), expr3b15))     ==  Right(constant(5)))

  }
}