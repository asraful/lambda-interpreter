/**
 * Created by Asraful on 3/27/2014.
 */

package project3b

import structures.ExprFactory._
import behaviors._


object fixtures {

  val Y         = fun("G", app(fun("g", app(variable("G"), app(variable("g"), variable("g")))), fun("g", app(variable("G"), app(variable("g"), variable("g"))))))
  val preFac    = fun("f", fun("n", iff(variable("n"), times(variable("n"), app(variable("f"), minus(variable("n"), constant(1)))), constant(1))))
  val preLength = fun("f", fun("n", iff(isCell(variable("n")), plus(constant(1), app(variable("f"), tl(variable("n")))), constant(0))))
  val preSize   = fun("f", fun("n", iff(isCell(variable("n")), plus(constant(1), plus(app(variable("f"), hd(variable("n"))), app(variable("f"), tl(variable("n"))))), constant(0))))

  
  /*
  ======================================== Project3a ========================================================
  */
  val expr3a1  = constant(3)
  val expr3a2  = variable("x")
  val expr3a3  = fun("x", plus(constant(3), variable("x")))
  val expr3a4  = app(fun("x", plus(constant(7), variable("x"))), constant(3))
  val expr3a5  = app(variable("x"), constant(3))
  val expr3a6  = iff(constant(7), constant(3), constant(4))
  val expr3a7  = iff(constant(0), constant(3), constant(4))
  val expr3a8  = iff(fun("x", variable("x")), constant(3), constant(4))
  val expr3a9  = iff(fun("x", variable("y")), constant(3), constant(4))
  //val expr3a10 = app(app(Y, fun("f", fun("n", iff(variable("n"), times(variable("n"), app(variable("f"), minus(variable("n"), constant(1)))), constant(1))))), constant(5))
  val expr3a10 = app(app(Y, preFac), constant(5))

  val fx = fun("x", iff(variable("x"), times(variable("x"), minus(variable("x"), constant(1))), constant(1)))
  val expr3a11 = app(fx, constant(5))
  val expr3a12 = app(expr3a11, expr3a11)



  /*
  ======================================== Project3b ========================================================
  */
  val expr3b1   =   cell(plus(constant(3), constant(7)), minus(constant(5), constant(2)))
  val expr3b2   =   hd(cell(plus(constant(3), constant(7)), minus(constant(5), constant(2))))
  val expr3b3   =   tl(cell(plus(constant(3), constant(7)), minus(constant(5), constant(2))))
  val expr3b4   =   iff(cell(plus(constant(3), constant(7)), minus(constant(5), constant(2))), constant(3), constant(4))
  val expr3b5   =   hd(constant(0))
  val expr3b6   =   tl(constant(0))
  val expr3b7   =   tl(fun("x", variable("x")))
  val expr3b8   =   tl(cell(constant(10), fun("x", variable("x"))))


  //For length and size testing

  val expr3b9   =   cell(constant(3), constant(2))
  val expr3b10  =   cell(constant(3), cell(constant(5), constant(2)))
  val expr3b11  =   cell(constant(1), cell(constant(3), cell(constant(5), constant(2))))
  val expr3b12  =   cell(constant(1), cell(constant(3), cell(constant(5), cell(constant(3), constant(2)))))
  val expr3b13  =   cell(constant(1), plus(constant(3), cell(constant(5), constant(1))))
  val expr3b14  =   cell(cell(constant(10), constant(11)), cell(cell(constant(20), constant(21)), cell(cell(constant(30), constant(31)), constant(0))))
  val expr3b15  =   cell(constant(10), cell(constant(21), cell(constant(30), cell(constant(40), cell(constant(34), constant(0))))))

}